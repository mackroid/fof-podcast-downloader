#!/usr/bin/python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup

import urllib, urlparse

########### ##############

url  = "http://www.filipandfredrik.com/podcast-arkiv/"
page = urllib.urlopen(url)

soup = BeautifulSoup(page.read())
links = soup.select(".download")

test_file = urllib.URLopener()

download_amount = 10
amount_to_skip = 25

for link in links[amount_to_skip:]:
	
	if download_amount == 0:
		break

	split = urlparse.urlsplit(link['href'])
	episode_name = split.path.split("/")[-1]
	print "Downloading " + episode_name

	filename = "downloads/" + episode_name
	test_file.retrieve(link['href'], filename)
	print episode_name + " has finished downloading"
	
	download_amount -= 1
print "all downloads have been finished"